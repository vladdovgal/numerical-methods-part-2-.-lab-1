#include <iostream>
#include <cmath>
using namespace std;
double u(double t)
{
	double one = 1;
	double u = (-one / 9.00 * exp(-9.00 * t) + one / 9.00 * (90.00 + exp(-6)) / exp(3))*exp(-t*(t - 4.00));
	return u;
}
double fi(double t)
{
	double result = t * exp(-t * t);
	return result;
}
double psi(double t)
{
	double result = exp(-2 * t);
	return result;
}
double g(double t)
{
	double result = 2 * (t - 2);
	return result;
}
double f(double t, double y)
{
	double result = fi(t)*psi(t) - g(t)*y;
	return result;
}
double max(double a, double b)
{
	if (a > b) { return a; }
	else return b;
}
double min(double a, double b)
{
	if (a < b) { return a; }
	else return b;
}
int main()
{
	double p = 3; //порядок апроксимації
//1
	double t0 = 1.00;
	double T = 6.00;
	double y0 = 10.00;
	double eps = 0.00001; //точність
	double tau0 = 0.50; // крок
	double epsM = 0.000001; // точність
//2
	double t = t0; // точка
	double y = y0; 	
	double tau = tau0;
	double Emax = 0.00;
	double one = 1.00;
	cout << " t = " << t << "\t" << " y = " << y << "\t" << " u(t) = " << u(t) << "\t" << " |y - u(t)| = " << abs(y - u(t));
//3
p3:
	if (abs(T - t) < epsM)
	{
		// cout << endl << " Emax = " << Emax;
		// system("pause");
		// return 0;
		goto p10;
	}
//4
	if (t + tau > T)
	{
		tau = T - t;
	}
//5
	double v = y, t1 = t;
	//6
	double k1 = f(t, y);
	//7 
p7:
	double row2[2];
	row2[0] = 1.00, row2[1] = 1.00;
	double row3[3];
	row3[0] = one / 2.00; row3[1] = one / 4.00; row3[2] = one / 4.00;
	double row4[3];
	row4[0] = one / 2.00; row4[1] = one / 2.00; row4[2] = 0.00;
	double row5[3];
	row5[0] = one / 6.00; row5[1] = one / 6.00; row5[2] = 4.00 * one / 6.00;
	double k2 = f(t + row2[0] * tau, y + tau*row2[1] * k1);
	double k3 = f(t + row3[0] * tau, y + tau*row3[1] * k1 + tau*row3[2] * k2);
	double w = y + tau * (row4[0] * k1 + row4[1] * k2 + row4[2] * k3);
	y = y + tau * (row5[0] * k1 + row5[1] * k2 + row5[2] * k3);
//8
	double E = (abs(y - w)) / max(1.00, abs(y));
	double tauH = tau * min(5.00, max(0.10, 0.90*pow(eps / E, one / p + 1)));
//9
	if (E <= eps)
	{
		t = t + tau;
		cout << endl << " t = " << t << "\t y = " << y << "\t u(t) =" << u(t) << "\t |y - u(t)| = " << abs(y - u(t));
		tau = tauH;
		if (Emax < abs(y - u(t)))
		{
			Emax = abs(y - u(t));
		}
		goto p3;
	}
	else
	{
		y = v;
		t = t1;
		tau = tauH;
		goto p7;
	}
//10
p10:
	cout << endl << " Emax = " << Emax;
//11
	system("pause");
	return 0;
}